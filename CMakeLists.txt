cmake_minimum_required(VERSION 3.5.1)

project(Flute)

set(Flute_VERSION 1.0)

add_library(flute SHARED flute_new.cpp flute.h)
set_target_properties(flute PROPERTIES
    VERSION ${Flute_VERSION}
    SOVERSION ${Flute_VERSION}
)
target_include_directories(flute PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<INSTALL_INTERFACE:include>
)

add_library(flute_static STATIC flute_new.cpp flute.h)
target_include_directories(flute_static PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<INSTALL_INTERFACE:include>
)

# Install License
install(
    FILES license.txt
    RENAME copyright
    DESTINATION share/doc/Flute
)

# Install rule for target
install(
    TARGETS flute flute_static
    DESTINATION lib
    EXPORT FluteConfig
)

# Install rule for headers
install(
    FILES flute.h
    DESTINATION include
)

# Install look up tables
install(
    FILES PORT9.dat POWV9.dat
    DESTINATION share/Flute
)

# Install export
install(
    EXPORT FluteConfig
    NAMESPACE Flute::
    DESTINATION share/Flute/cmake
)
